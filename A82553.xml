<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A82553">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Junii 3. 1643. At the Committee of Lords and Commons for advance of money and other necessaries for the Army.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A82553 of text R211710 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.7[19]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A82553</idno>
    <idno type="STC">Wing E1243</idno>
    <idno type="STC">Thomason 669.f.7[19]</idno>
    <idno type="STC">ESTC R211710</idno>
    <idno type="EEBO-CITATION">99870416</idno>
    <idno type="PROQUEST">99870416</idno>
    <idno type="VID">161001</idno>
    <idno type="PROQUESTGOID">2240931542</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A82553)</note>
    <note>Transcribed from: (Early English Books Online ; image set 161001)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f7[19])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Junii 3. 1643. At the Committee of Lords and Commons for advance of money and other necessaries for the Army.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1643]</date>
     </publicationStmt>
     <notesStmt>
      <note>Imprint from Wing.</note>
      <note>An order respecting the collection of the weekly assessment.</note>
      <note>Signed: Martin Dallison, Clerke to the said Committee.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Taxation -- Great Britain -- 17th century -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
     <term>Great Britain -- Militia -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A82553</ep:tcp>
    <ep:estc> R211710</ep:estc>
    <ep:stc> (Thomason 669.f.7[19]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Junii 3. 1643. At the Committee of Lords and Commons for advance of money and other necessaries for the Army.</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1643</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>452</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-10</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-11</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-11</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A82553-e10">
  <body xml:id="A82553-e20">
   <pb facs="tcp:161001:1" rend="simple:additions" xml:id="A82553-001-a"/>
   <div type="text" xml:id="A82553-e30">
    <opener xml:id="A82553-e40">
     <dateline xml:id="A82553-e50">
      <date xml:id="A82553-e60">
       <w lemma="n/a" pos="fla" rend="hi" xml:id="A82553-001-a-0010">Junij</w>
       <w lemma="3." pos="crd" xml:id="A82553-001-a-0020">3.</w>
       <w lemma="1643." pos="crd" xml:id="A82553-001-a-0030">1643.</w>
       <pc unit="sentence" xml:id="A82553-001-a-0040"/>
      </date>
     </dateline>
    </opener>
    <head xml:id="A82553-e80">
     <w lemma="at" pos="acp" xml:id="A82553-001-a-0050">At</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0060">the</w>
     <w lemma="committee" pos="n1" xml:id="A82553-001-a-0070">Committee</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0080">of</w>
     <w lemma="lord" pos="n2" xml:id="A82553-001-a-0090">Lords</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-0100">and</w>
     <w lemma="commons" pos="n2" xml:id="A82553-001-a-0110">Commons</w>
     <w lemma="for" pos="acp" xml:id="A82553-001-a-0120">for</w>
     <w lemma="advance" pos="vvi" xml:id="A82553-001-a-0130">Advance</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0140">of</w>
     <w lemma="money" pos="n1" xml:id="A82553-001-a-0150">Money</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-0160">and</w>
     <w lemma="other" pos="pi-d" xml:id="A82553-001-a-0170">other</w>
     <w lemma="necessary" pos="n2-j" xml:id="A82553-001-a-0180">Necessaries</w>
     <w lemma="for" pos="acp" xml:id="A82553-001-a-0190">for</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0200">the</w>
     <w lemma="army" pos="n1" xml:id="A82553-001-a-0210">ARMY</w>
     <pc unit="sentence" xml:id="A82553-001-a-0220">.</pc>
    </head>
    <p xml:id="A82553-e90">
     <w lemma="it" pos="pn" rend="decorinit" xml:id="A82553-001-a-0230">IT</w>
     <w lemma="be" pos="vvz" xml:id="A82553-001-a-0240">is</w>
     <w lemma="order" pos="vvn" xml:id="A82553-001-a-0250">Ordered</w>
     <pc xml:id="A82553-001-a-0260">,</pc>
     <w lemma="that" pos="cs" xml:id="A82553-001-a-0270">That</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0280">the</w>
     <w lemma="collector" pos="n2" xml:id="A82553-001-a-0290">Collectors</w>
     <w lemma="upon" pos="acp" xml:id="A82553-001-a-0300">upon</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0310">the</w>
     <w lemma="weekly" pos="j" xml:id="A82553-001-a-0320">weekly</w>
     <w lemma="assessment" pos="n1" reg="assessment" xml:id="A82553-001-a-0330">Assessement</w>
     <pc xml:id="A82553-001-a-0340">,</pc>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-0350">by</w>
     <w lemma="virtue" pos="n1" reg="virtue" xml:id="A82553-001-a-0360">vertue</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0370">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0380">the</w>
     <w lemma="ordinance" pos="n1" xml:id="A82553-001-a-0390">Ordinance</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0400">of</w>
     <w lemma="parliament" pos="n1" xml:id="A82553-001-a-0410">Parliament</w>
     <pc xml:id="A82553-001-a-0420">,</pc>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0430">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0440">the</w>
     <w lemma="four" pos="ord" xml:id="A82553-001-a-0450">fourth</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0460">of</w>
     <w lemma="march" pos="nn1" rend="hi" xml:id="A82553-001-a-0470">March</w>
     <w lemma="last" pos="ord" xml:id="A82553-001-a-0480">last</w>
     <pc xml:id="A82553-001-a-0490">,</pc>
     <w lemma="do" pos="vvb" reg="do" xml:id="A82553-001-a-0500">doe</w>
     <w lemma="use" pos="vvi" xml:id="A82553-001-a-0510">use</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-0520">their</w>
     <w lemma="best" pos="js" xml:id="A82553-001-a-0530">best</w>
     <w lemma="endeavour" pos="n2" reg="endeavours" xml:id="A82553-001-a-0540">indeavours</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-0550">to</w>
     <w lemma="collect" pos="vvi" xml:id="A82553-001-a-0560">collect</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0570">the</w>
     <w lemma="arrear" pos="n2" reg="arrears" xml:id="A82553-001-a-0580">Arreares</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0590">of</w>
     <w lemma="such" pos="d" xml:id="A82553-001-a-0600">such</w>
     <w lemma="money" pos="n2" reg="moneys" xml:id="A82553-001-a-0610">monies</w>
     <w lemma="as" pos="acp" xml:id="A82553-001-a-0620">as</w>
     <w lemma="be" pos="vvb" xml:id="A82553-001-a-0630">are</w>
     <w lemma="assess" pos="vvn" xml:id="A82553-001-a-0640">assessed</w>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-0650">by</w>
     <w lemma="virtue" pos="n1" reg="virtue" xml:id="A82553-001-a-0660">vertue</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0670">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0680">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-0690">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A82553-001-a-0700">Ordinance</w>
     <pc xml:id="A82553-001-a-0710">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-0720">and</w>
     <w lemma="carry" pos="vvi" xml:id="A82553-001-a-0730">carry</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-0740">in</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-0750">and</w>
     <w lemma="pay" pos="vvi" xml:id="A82553-001-a-0760">pay</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0770">the</w>
     <w lemma="same" pos="d" xml:id="A82553-001-a-0780">same</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-0790">to</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0800">the</w>
     <w lemma="treasurer" pos="n2" xml:id="A82553-001-a-0810">Treasurers</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-0820">in</w>
     <hi xml:id="A82553-e110">
      <w lemma="Guildhall" pos="nn1" reg="Guildhall" xml:id="A82553-001-a-0830">Guild-hall</w>
      <pc xml:id="A82553-001-a-0840">,</pc>
     </hi>
     <w lemma="before" pos="acp" xml:id="A82553-001-a-0850">before</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0860">the</w>
     <w lemma="twelve" pos="ord" xml:id="A82553-001-a-0870">twelfth</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-0880">of</w>
     <w lemma="this" pos="d" xml:id="A82553-001-a-0890">this</w>
     <w lemma="instant" pos="j" xml:id="A82553-001-a-0900">instant</w>
     <hi xml:id="A82553-e120">
      <w lemma="June" pos="nn1" xml:id="A82553-001-a-0910">June</w>
      <pc xml:id="A82553-001-a-0920">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-0930">&amp;</w>
     <w lemma="that" pos="cs" xml:id="A82553-001-a-0940">that</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-0950">in</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0960">the</w>
     <w lemma="mean" pos="j" reg="mean" xml:id="A82553-001-a-0970">meane</w>
     <w lemma="time" pos="n1" xml:id="A82553-001-a-0980">time</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-0990">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-1000">said</w>
     <w lemma="collector" pos="n2" xml:id="A82553-001-a-1010">Collectors</w>
     <w lemma="do" pos="vvb" xml:id="A82553-001-a-1020">do</w>
     <w lemma="give" pos="vvi" xml:id="A82553-001-a-1030">give</w>
     <w lemma="a" pos="d" xml:id="A82553-001-a-1040">a</w>
     <w lemma="note" pos="n1" xml:id="A82553-001-a-1050">note</w>
     <pc join="right" xml:id="A82553-001-a-1060">(</pc>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-1070">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1080">the</w>
     <w lemma="name" pos="n2" xml:id="A82553-001-a-1090">names</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-1100">of</w>
     <w lemma="such" pos="d" xml:id="A82553-001-a-1110">such</w>
     <w lemma="person" pos="n2" xml:id="A82553-001-a-1120">persons</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-1130">in</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-1140">their</w>
     <w lemma="several" pos="j" reg="several" xml:id="A82553-001-a-1150">severall</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1160">and</w>
     <w lemma="respective" pos="j" xml:id="A82553-001-a-1170">respective</w>
     <w lemma="ward" pos="n2" xml:id="A82553-001-a-1180">Wards</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1190">and</w>
     <w lemma="Precinct" pos="n2" xml:id="A82553-001-a-1200">Precincts</w>
     <w lemma="as" pos="acp" xml:id="A82553-001-a-1210">as</w>
     <w lemma="have" pos="vvi" xml:id="A82553-001-a-1220">have</w>
     <w lemma="be" pos="vvn" xml:id="A82553-001-a-1230">been</w>
     <w lemma="refractory" pos="j" xml:id="A82553-001-a-1240">refractory</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1250">and</w>
     <w lemma="refuse" pos="vvi" xml:id="A82553-001-a-1260">refuse</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-1270">to</w>
     <w lemma="pay" pos="vvi" xml:id="A82553-001-a-1280">pay</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1290">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-1300">said</w>
     <w lemma="weekly" pos="j" xml:id="A82553-001-a-1310">weekly</w>
     <w lemma="assessment" pos="n1" reg="assessment" xml:id="A82553-001-a-1320">assessement</w>
     <pc xml:id="A82553-001-a-1330">)</pc>
     <w lemma="unto" pos="acp" xml:id="A82553-001-a-1340">unto</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1350">the</w>
     <w lemma="person" pos="n2" xml:id="A82553-001-a-1360">persons</w>
     <w lemma="new" pos="av-j" xml:id="A82553-001-a-1370">newly</w>
     <w lemma="appoint" pos="vvn" xml:id="A82553-001-a-1380">appointed</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-1390">to</w>
     <w lemma="take" pos="vvi" xml:id="A82553-001-a-1400">take</w>
     <w lemma="distress" pos="n1" reg="distress" xml:id="A82553-001-a-1410">distresse</w>
     <pc xml:id="A82553-001-a-1420">,</pc>
     <w lemma="who" pos="crq" xml:id="A82553-001-a-1430">who</w>
     <w lemma="be" pos="vvb" xml:id="A82553-001-a-1440">are</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-1450">to</w>
     <w lemma="distrain" pos="vvi" reg="distrain" xml:id="A82553-001-a-1460">distraine</w>
     <w lemma="for" pos="acp" xml:id="A82553-001-a-1470">for</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1480">the</w>
     <w lemma="sum" pos="n2" reg="sums" xml:id="A82553-001-a-1490">summes</w>
     <w lemma="assess" pos="vvn" xml:id="A82553-001-a-1500">assessed</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1510">and</w>
     <w lemma="unpaid" pos="j" xml:id="A82553-001-a-1520">unpaid</w>
     <pc xml:id="A82553-001-a-1530">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1540">and</w>
     <w lemma="pay" pos="vvi" xml:id="A82553-001-a-1550">pay</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1560">the</w>
     <w lemma="same" pos="d" xml:id="A82553-001-a-1570">same</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-1580">to</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1590">the</w>
     <w lemma="collector" pos="n2" xml:id="A82553-001-a-1600">Collectors</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-1610">in</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1620">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-1630">said</w>
     <w lemma="several" pos="j" reg="several" xml:id="A82553-001-a-1640">severall</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1650">and</w>
     <w lemma="respective" pos="j" xml:id="A82553-001-a-1660">respective</w>
     <w lemma="ward" pos="n2" xml:id="A82553-001-a-1670">Wards</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-1680">in</w>
     <w lemma="case" pos="n1" xml:id="A82553-001-a-1690">case</w>
     <w lemma="they" pos="pns" xml:id="A82553-001-a-1700">they</w>
     <w lemma="distrain" pos="vvb" reg="distrain" xml:id="A82553-001-a-1710">distraine</w>
     <w lemma="money" pos="n1" reg="money" xml:id="A82553-001-a-1720">mony</w>
     <pc xml:id="A82553-001-a-1730">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1740">and</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1750">the</w>
     <w lemma="good" pos="n2-j" xml:id="A82553-001-a-1760">goods</w>
     <w lemma="they" pos="pns" xml:id="A82553-001-a-1770">they</w>
     <w lemma="distrain" pos="vvb" reg="distrain" xml:id="A82553-001-a-1780">distraine</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-1790">to</w>
     <w lemma="carry" pos="vvi" xml:id="A82553-001-a-1800">carry</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-1810">to</w>
     <w lemma="Guildhall" pos="nn1" reg="Guildhall" rend="hi" xml:id="A82553-001-a-1820">Guild-hall</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-1830">to</w>
     <hi xml:id="A82553-e140">
      <w lemma="Samuel" pos="nn1" xml:id="A82553-001-a-1840">Samuel</w>
      <w lemma="Gosse" pos="nn1" xml:id="A82553-001-a-1850">Gosse</w>
      <pc xml:id="A82553-001-a-1860">,</pc>
     </hi>
     <w lemma="or" pos="cc" xml:id="A82553-001-a-1870">or</w>
     <w lemma="his" pos="po" xml:id="A82553-001-a-1880">his</w>
     <w lemma="deputy" pos="n1" xml:id="A82553-001-a-1890">Deputy</w>
     <w lemma="appoint" pos="vvn" xml:id="A82553-001-a-1900">appointed</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-1910">to</w>
     <w lemma="receive" pos="vvi" xml:id="A82553-001-a-1920">receive</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-1930">the</w>
     <w lemma="same" pos="d" xml:id="A82553-001-a-1940">same</w>
     <pc unit="sentence" xml:id="A82553-001-a-1950">.</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-1960">And</w>
     <w lemma="for" pos="acp" xml:id="A82553-001-a-1970">for</w>
     <w lemma="such" pos="d" xml:id="A82553-001-a-1980">such</w>
     <w lemma="sum" pos="n2" xml:id="A82553-001-a-1990">sums</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-2000">of</w>
     <w lemma="money" pos="n1" xml:id="A82553-001-a-2010">money</w>
     <w lemma="as" pos="acp" xml:id="A82553-001-a-2020">as</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2030">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-2040">said</w>
     <w lemma="person" pos="n2" xml:id="A82553-001-a-2050">persons</w>
     <w lemma="shall" pos="vmb" xml:id="A82553-001-a-2060">shall</w>
     <w lemma="distrain" pos="vvi" reg="distrain" xml:id="A82553-001-a-2070">distraine</w>
     <pc xml:id="A82553-001-a-2080">,</pc>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2090">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-2100">said</w>
     <w lemma="collector" pos="n2" xml:id="A82553-001-a-2110">Collectors</w>
     <w lemma="shall" pos="vmb" xml:id="A82553-001-a-2120">shall</w>
     <w lemma="not" pos="xx" xml:id="A82553-001-a-2130">not</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A82553-001-a-2140">onely</w>
     <w lemma="allow" pos="vvi" xml:id="A82553-001-a-2150">allow</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-2160">and</w>
     <w lemma="pay" pos="vvi" xml:id="A82553-001-a-2170">pay</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-2180">to</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2190">the</w>
     <w lemma="person" pos="n2" xml:id="A82553-001-a-2200">persons</w>
     <w lemma="take" pos="vvg" xml:id="A82553-001-a-2210">taking</w>
     <w lemma="distress" pos="n1" reg="distress" xml:id="A82553-001-a-2220">distresse</w>
     <pc xml:id="A82553-001-a-2230">,</pc>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2240">the</w>
     <w lemma="two" pos="crd" xml:id="A82553-001-a-2250">two</w>
     <w lemma="penny" pos="n2" xml:id="A82553-001-a-2260">pence</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-2270">in</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2280">the</w>
     <w lemma="pound" pos="n1" xml:id="A82553-001-a-2290">pound</w>
     <w lemma="allow" pos="vvn" xml:id="A82553-001-a-2300">allowed</w>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-2310">by</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2320">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-2330">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A82553-001-a-2340">Ordinance</w>
     <pc xml:id="A82553-001-a-2350">,</pc>
     <w lemma="but" pos="acp" xml:id="A82553-001-a-2360">but</w>
     <w lemma="also" pos="av" xml:id="A82553-001-a-2370">also</w>
     <w lemma="two" pos="crd" xml:id="A82553-001-a-2380">two</w>
     <w lemma="penny" pos="n2" xml:id="A82553-001-a-2390">pence</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-2400">in</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2410">the</w>
     <w lemma="pound" pos="n1" xml:id="A82553-001-a-2420">pound</w>
     <w lemma="more" pos="avc-d" xml:id="A82553-001-a-2430">more</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-2440">of</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-2450">their</w>
     <w lemma="own" pos="d" xml:id="A82553-001-a-2460">own</w>
     <w lemma="money" pos="n2" reg="moneys" xml:id="A82553-001-a-2470">monies</w>
     <w lemma="for" pos="acp" xml:id="A82553-001-a-2480">for</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-2490">their</w>
     <w lemma="pain" pos="n2" reg="pains" xml:id="A82553-001-a-2500">paines</w>
     <w lemma="therein" pos="av" xml:id="A82553-001-a-2510">therein</w>
     <pc xml:id="A82553-001-a-2520">,</pc>
     <w lemma="if" pos="cs" xml:id="A82553-001-a-2530">if</w>
     <w lemma="they" pos="pns" xml:id="A82553-001-a-2540">they</w>
     <w lemma="themselves" pos="pr" xml:id="A82553-001-a-2550">themselves</w>
     <w lemma="shall" pos="vmb" xml:id="A82553-001-a-2560">shall</w>
     <w lemma="not" pos="xx" xml:id="A82553-001-a-2570">not</w>
     <w lemma="be" pos="vvi" xml:id="A82553-001-a-2580">be</w>
     <w lemma="willing" pos="j" xml:id="A82553-001-a-2590">willing</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-2600">to</w>
     <w lemma="perform" pos="vvi" reg="perform" xml:id="A82553-001-a-2610">performe</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2620">the</w>
     <w lemma="duty" pos="n1" xml:id="A82553-001-a-2630">duty</w>
     <w lemma="as" pos="acp" xml:id="A82553-001-a-2640">as</w>
     <w lemma="be" pos="vvz" xml:id="A82553-001-a-2650">is</w>
     <w lemma="require" pos="vvn" xml:id="A82553-001-a-2660">required</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-2670">of</w>
     <w lemma="they" pos="pno" xml:id="A82553-001-a-2680">them</w>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-2690">by</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2700">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-2710">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A82553-001-a-2720">Ordinance</w>
     <pc xml:id="A82553-001-a-2730">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-2740">And</w>
     <w lemma="that" pos="cs" xml:id="A82553-001-a-2750">that</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2760">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-2770">said</w>
     <w lemma="collector" pos="n2" xml:id="A82553-001-a-2780">Collectors</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A82553-001-a-2790">doe</w>
     <w lemma="appear" pos="vvi" reg="appear" xml:id="A82553-001-a-2800">appeare</w>
     <w lemma="before" pos="acp" xml:id="A82553-001-a-2810">before</w>
     <w lemma="this" pos="d" xml:id="A82553-001-a-2820">this</w>
     <w lemma="committee" pos="n1" xml:id="A82553-001-a-2830">Committee</w>
     <w lemma="on" pos="acp" xml:id="A82553-001-a-2840">on</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2850">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-2860">said</w>
     <w lemma="twelve" pos="n1" xml:id="A82553-001-a-2870">twelfth</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-2880">of</w>
     <hi xml:id="A82553-e150">
      <w lemma="June" pos="nn1" xml:id="A82553-001-a-2890">June</w>
      <pc xml:id="A82553-001-a-2900">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-2910">and</w>
     <w lemma="bring" pos="vvi" xml:id="A82553-001-a-2920">bring</w>
     <w lemma="with" pos="acp" xml:id="A82553-001-a-2930">with</w>
     <w lemma="they" pos="pno" xml:id="A82553-001-a-2940">them</w>
     <w lemma="a" pos="d" xml:id="A82553-001-a-2950">a</w>
     <w lemma="note" pos="n1" xml:id="A82553-001-a-2960">note</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-2970">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-2980">the</w>
     <w lemma="total" pos="j" reg="total" xml:id="A82553-001-a-2990">totall</w>
     <w lemma="sum" pos="n2" reg="sums" xml:id="A82553-001-a-3000">summes</w>
     <w lemma="assess" pos="vvn" xml:id="A82553-001-a-3010">assessed</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-3020">in</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-3030">their</w>
     <w lemma="several" pos="j" reg="several" xml:id="A82553-001-a-3040">severall</w>
     <w lemma="precinct" pos="n2" xml:id="A82553-001-a-3050">precincts</w>
     <pc xml:id="A82553-001-a-3060">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-3070">and</w>
     <w lemma="commit" pos="vvn" xml:id="A82553-001-a-3080">committed</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-3090">to</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-3100">their</w>
     <w lemma="care" pos="n1" xml:id="A82553-001-a-3110">care</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3120">of</w>
     <w lemma="collection" pos="n1" xml:id="A82553-001-a-3130">Collection</w>
     <pc xml:id="A82553-001-a-3140">;</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-3150">And</w>
     <w lemma="a" pos="d" xml:id="A82553-001-a-3160">an</w>
     <w lemma="account" pos="n1" reg="account" xml:id="A82553-001-a-3170">Accompt</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3180">of</w>
     <w lemma="what" pos="crq" xml:id="A82553-001-a-3190">what</w>
     <w lemma="sum" pos="n2" reg="sums" xml:id="A82553-001-a-3200">summes</w>
     <w lemma="they" pos="pns" xml:id="A82553-001-a-3210">they</w>
     <w lemma="have" pos="vvb" xml:id="A82553-001-a-3220">have</w>
     <w lemma="pay" pos="vvn" xml:id="A82553-001-a-3230">paid</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-3240">to</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3250">the</w>
     <w lemma="treasurer" pos="n2" xml:id="A82553-001-a-3260">Treasurers</w>
     <w lemma="at" pos="acp" xml:id="A82553-001-a-3270">at</w>
     <w lemma="Guildhall" pos="nn1" reg="Guildhall" rend="hi" xml:id="A82553-001-a-3280">Guild-hall</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3290">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3300">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-3310">said</w>
     <w lemma="weekly" pos="j" xml:id="A82553-001-a-3320">weekly</w>
     <w lemma="assessment" pos="n1" reg="assessment" xml:id="A82553-001-a-3330">assessement</w>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-3340">by</w>
     <w lemma="they" pos="pno" xml:id="A82553-001-a-3350">them</w>
     <w lemma="respective" pos="av-j" xml:id="A82553-001-a-3360">respectively</w>
     <w lemma="collect" pos="vvn" xml:id="A82553-001-a-3370">collected</w>
     <pc xml:id="A82553-001-a-3380">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-3390">and</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3400">the</w>
     <w lemma="date" pos="n2" xml:id="A82553-001-a-3410">dates</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3420">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3430">the</w>
     <w lemma="acquittance" pos="n2" xml:id="A82553-001-a-3440">Acquittances</w>
     <w lemma="give" pos="vvn" xml:id="A82553-001-a-3450">given</w>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-3460">by</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3470">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-3480">said</w>
     <w lemma="treasurer" pos="n2" xml:id="A82553-001-a-3490">Treasurers</w>
     <w lemma="for" pos="acp" xml:id="A82553-001-a-3500">for</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3510">the</w>
     <w lemma="same" pos="d" xml:id="A82553-001-a-3520">same</w>
     <pc xml:id="A82553-001-a-3530">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-3540">and</w>
     <w lemma="what" pos="crq" xml:id="A82553-001-a-3550">what</w>
     <w lemma="sum" pos="n2" xml:id="A82553-001-a-3560">sums</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3570">of</w>
     <w lemma="money" pos="n1" xml:id="A82553-001-a-3580">money</w>
     <w lemma="shall" pos="vmb" xml:id="A82553-001-a-3590">shall</w>
     <w lemma="then" pos="av" xml:id="A82553-001-a-3600">then</w>
     <w lemma="remain" pos="vvi" reg="remain" xml:id="A82553-001-a-3610">remaine</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-3620">in</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-3630">their</w>
     <w lemma="hand" pos="n2" xml:id="A82553-001-a-3640">hands</w>
     <w lemma="not" pos="xx" xml:id="A82553-001-a-3650">not</w>
     <w lemma="pay" pos="vvn" xml:id="A82553-001-a-3660">paid</w>
     <w lemma="in" pos="acp" xml:id="A82553-001-a-3670">in</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-3680">to</w>
     <hi xml:id="A82553-e170">
      <w lemma="Guildhall" pos="nn1" reg="Guildhall" xml:id="A82553-001-a-3690">Guild-hall</w>
      <pc xml:id="A82553-001-a-3700">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-3710">And</w>
     <w lemma="a" pos="d" xml:id="A82553-001-a-3720">a</w>
     <w lemma="roll" pos="n1" xml:id="A82553-001-a-3730">Roll</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3740">of</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3750">the</w>
     <w lemma="name" pos="n2" xml:id="A82553-001-a-3760">names</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3770">of</w>
     <w lemma="such" pos="d" xml:id="A82553-001-a-3780">such</w>
     <w lemma="person" pos="n2" xml:id="A82553-001-a-3790">persons</w>
     <pc xml:id="A82553-001-a-3800">,</pc>
     <w lemma="as" pos="acp" xml:id="A82553-001-a-3810">as</w>
     <w lemma="then" pos="av" xml:id="A82553-001-a-3820">then</w>
     <w lemma="shall" pos="vmb" xml:id="A82553-001-a-3830">shall</w>
     <w lemma="not" pos="xx" xml:id="A82553-001-a-3840">not</w>
     <w lemma="have" pos="vvi" xml:id="A82553-001-a-3850">have</w>
     <w lemma="full" pos="av-j" xml:id="A82553-001-a-3860">fully</w>
     <w lemma="pay" pos="vvn" xml:id="A82553-001-a-3870">paid</w>
     <w lemma="their" pos="po" xml:id="A82553-001-a-3880">their</w>
     <w lemma="assessment" pos="n2" reg="assessments" xml:id="A82553-001-a-3890">assessements</w>
     <pc xml:id="A82553-001-a-3900">,</pc>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-3910">and</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-3920">the</w>
     <w lemma="several" pos="j" reg="several" xml:id="A82553-001-a-3930">severall</w>
     <w lemma="sum" pos="n2" reg="sums" xml:id="A82553-001-a-3940">summes</w>
     <w lemma="of" pos="acp" xml:id="A82553-001-a-3950">of</w>
     <w lemma="money" pos="n1" xml:id="A82553-001-a-3960">money</w>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-3970">by</w>
     <w lemma="they" pos="pno" xml:id="A82553-001-a-3980">them</w>
     <w lemma="unpaid" pos="j" xml:id="A82553-001-a-3990">unpaid</w>
     <pc xml:id="A82553-001-a-4000">;</pc>
     <w lemma="that" pos="cs" xml:id="A82553-001-a-4010">That</w>
     <w lemma="so" pos="av" xml:id="A82553-001-a-4020">so</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-4030">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-4040">said</w>
     <w lemma="collector" pos="n2" xml:id="A82553-001-a-4050">Collectors</w>
     <pc xml:id="A82553-001-a-4060">,</pc>
     <w lemma="or" pos="cc" xml:id="A82553-001-a-4070">or</w>
     <w lemma="those" pos="d" xml:id="A82553-001-a-4080">those</w>
     <w lemma="person" pos="n2" xml:id="A82553-001-a-4090">persons</w>
     <w lemma="new" pos="av-j" xml:id="A82553-001-a-4100">newly</w>
     <w lemma="appoint" pos="vvn" xml:id="A82553-001-a-4110">appointed</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-4120">to</w>
     <w lemma="distrain" pos="vvi" reg="distrain" xml:id="A82553-001-a-4130">distraine</w>
     <pc xml:id="A82553-001-a-4140">,</pc>
     <w lemma="may" pos="vmb" xml:id="A82553-001-a-4150">may</w>
     <w lemma="levy" pos="vvi" xml:id="A82553-001-a-4160">levy</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-4170">the</w>
     <w lemma="same" pos="d" xml:id="A82553-001-a-4180">same</w>
     <w lemma="by" pos="acp" xml:id="A82553-001-a-4190">by</w>
     <w lemma="distress" pos="n1" reg="distress" xml:id="A82553-001-a-4200">distresse</w>
     <w lemma="according" pos="j" xml:id="A82553-001-a-4210">according</w>
     <w lemma="to" pos="acp" xml:id="A82553-001-a-4220">to</w>
     <w lemma="the" pos="d" xml:id="A82553-001-a-4230">the</w>
     <w lemma="say" pos="j-vn" xml:id="A82553-001-a-4240">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A82553-001-a-4250">Ordinance</w>
     <pc xml:id="A82553-001-a-4260">,</pc>
     <w lemma="whereof" pos="crq" xml:id="A82553-001-a-4270">whereof</w>
     <w lemma="they" pos="pns" xml:id="A82553-001-a-4280">they</w>
     <w lemma="be" pos="vvb" xml:id="A82553-001-a-4290">are</w>
     <w lemma="not" pos="xx" xml:id="A82553-001-a-4300">not</w>
     <w lemma="to" pos="prt" xml:id="A82553-001-a-4310">to</w>
     <w lemma="fail" pos="vvi" reg="fail" xml:id="A82553-001-a-4320">faile</w>
     <pc unit="sentence" xml:id="A82553-001-a-4330">.</pc>
     <w lemma="it" pos="pn" xml:id="A82553-001-a-4340">It</w>
     <w lemma="be" pos="vvz" xml:id="A82553-001-a-4350">is</w>
     <w lemma="also" pos="av" xml:id="A82553-001-a-4360">also</w>
     <w lemma="order" pos="vvn" xml:id="A82553-001-a-4370">ordered</w>
     <w lemma="that" pos="cs" xml:id="A82553-001-a-4380">that</w>
     <w lemma="this" pos="d" xml:id="A82553-001-a-4390">this</w>
     <w lemma="be" pos="vvb" xml:id="A82553-001-a-4400">be</w>
     <w lemma="print" pos="vvn" xml:id="A82553-001-a-4410">Printed</w>
     <w lemma="and" pos="cc" xml:id="A82553-001-a-4420">and</w>
     <w lemma="publish" pos="vvn" xml:id="A82553-001-a-4430">published</w>
     <pc unit="sentence" xml:id="A82553-001-a-4440">.</pc>
    </p>
    <closer xml:id="A82553-e180">
     <signed xml:id="A82553-e190">
      <w lemma="Martin" pos="nn1" xml:id="A82553-001-a-4450">Martin</w>
      <w lemma="Dallison" pos="nn1" xml:id="A82553-001-a-4460">Dallison</w>
      <w lemma="clerk" pos="n1" reg="clerk" xml:id="A82553-001-a-4470">Clerke</w>
      <w lemma="to" pos="acp" xml:id="A82553-001-a-4480">to</w>
      <w lemma="the" pos="d" xml:id="A82553-001-a-4490">the</w>
      <w lemma="say" pos="j-vn" xml:id="A82553-001-a-4500">said</w>
      <w lemma="committee" pos="n1" xml:id="A82553-001-a-4510">Committee</w>
      <pc unit="sentence" xml:id="A82553-001-a-4520">.</pc>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>
